# spring-examples

> A collection of Spring Boot example controllers, services, repositories and more

## Licence

[Unlicense](https://spdx.org/licenses/Unlicense.html) — released into the public domain.
